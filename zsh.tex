\documentclass{lug}

\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}
\newcommand{\zshrc}{\texttt{\textasciitilde/.zshrc}}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\title{systemd}
\author{Ross Starritt}
\institute{Mines Linux Users Group}

\begin{document}

\section{Basics}

\begin{frame}[fragile]{What is \texttt{systemd}?}
	\texttt{systemd} builds your OS and then manages your daemons\\
	Includes dependency management
    \pause

    \textbf{And most of the people here at LUG use it}
    \pause
    
    It is the default init program in most major distros
    \pause
	
	Runs as PID 1 when used as init
	\pause
	
	Composed of 69 individual binaries    
	\pause
	
	Log everything that happens
	\pause
	
	Runs as a daemon itself

\end{frame}

\begin{frame}{Philosophy of \texttt{systemd}}
	Every system service is a daemon encapsulated within a unit
	\pause
	
	System manager, not just service manager
	\pause
	
	Bridge gap between userspace and kernelspace
	\pause
	
	Distro agnostic, but Linux kernel specific
	\pause
	
	Handle system events

\end{frame}

\begin{frame}{Ancestry}
	\texttt{systemd} was created to address perceived deficiencies in SysV
	\pause
	
	The old system ran a series of bash scripts , which were written by distro maintainers
	\pause
	
	Windows had SVChost and Apple had launchd \\
	Both were successful programs based on services
	\pause
	
	Created in 2010, and saw wide adoption by 2015
\end{frame}

\begin{frame}{Structure}
	Daemons wrapped in units
	\pause
	
	Track units with cgroups
	\pause
	
	Units grouped into targets
	e.g. graphical.target launches everything needed to run a GUI
	\pause
	
	Communicate with sockets
	\pause
	
	Units can request state change of other units with jobs
	\pause
	
	Run transaction on jobs before running the job
\end{frame}

\begin{frame}{Transactions}
	Don't destroy the current state
	\pause
	
	\begin{enumerate}
		\item Check for requested state change
		\pause
		
		\item Check for internal conflict and loops
		\pause
		
		\item Check with conflicts with the preexisting job queue
		\pause
		
		\item Merge with the job queue
		
	\end{enumerate}
	\pause
	
	\texttt{systemd} will attempt to solve any of the above issues\\
	Jobs are only declined if resolution is impossible.
\end{frame}

\begin{frame}{Unit Types}
	\begin{enumerate}
\item		service - start and control daemons and their processes\\
\pause
\item 		socket - deal with IPC and networking sockets and socket-based activation\\
\pause
\item		target - group of units\\
\pause
\item 		device - expose kernel devices\\
\pause
\item		moint - control file system mount points\\
\pause
\item 		automount - allows parallelized boot and on-demand filesystem mounting\\
\pause
\item 		timer - trigger other units based on timers -- replace cron\\
\pause
\item 		swap - encapsulate memory swap\\
\pause
\item 		path - activate a service when an object is changed on file system\\
\pause
\item		slice - group units to manage processes in a hierarchical tree (for resource management)\\
\pause
\item 		scope - manage foreign processes (no starting)
		
	\end{enumerate}
\end{frame}

\section{Usage}

\begin{frame}[fragile]{Daemon Management}
    With \mintinline{bash}{systemctl}, units can be manipulated\\
    \pause
    \mintinline{bash}{systemctl status myservice} will tell you the state of myservice\\
    \pause
    Units can be started and stopped for the current session\\
    Replace "status" with "start" or "stop"\\
    \pause
    To start a unit on every boot or prevent a unit from starting\\
    Replace with "enable" or "disable"
     
\end{frame} 

\begin{frame}{\texttt{journalctl}}
	Read the logs!\\
	\pause
	\texttt{systemd} uses append only logging. Logs are persistent by default\\
	\pause
	\mintinline{bash}{journalctl -b} returns the current boot\\
	\pause
	\mintinline{bash}{journalctl -b -1} returns the previous boot\\
	\pause
	\mintinline{bash}{journalctl -D /mnt/var/log/journal -xe} allows you to read logs from another system, mounted at /mnt\\
	\pause
	logging options set by environment variables
	
\end{frame}

\begin{frame}{Power Management}
	\texttt{systemd} provides power management targets. \\
	They are invoked  with \mintinline{bash}{systemctl}\\
	\pause
	\begin{itemize}
		\item poweroff\\
		\item reboot\\
		\item suspend\\
		\item hibernate\\
		\item hybrid-sleep\\
	\end{itemize}
\end{frame}

\begin{frame}{Timers}
	Realtime and monotonic timers are supported\\
	Archwiki has examples!\\
	\pause
	\mintinline{bash}{systemd-run} allows arbitrary commands to be ran after a timer\\
	\pause
	As a cron replacement:\\
	\pause
	Pros:\\
	+ logging\\
	+ systemd benefits\\
	\pause
	Cons:\\
	- more complicated setup\\
	- MAILTO functionality missing. Can be shoehorned in\\
	\pause
	Programs exist to translate from crontabs to \texttt{systemd} timers
	
\end{frame}

\begin{frame}{Drama}
	Debian:\\
	In 2014, the Debian mailing list was subjected to heated discussion about whether or not to change over to \texttt{systemd}\\
	\pause
	Four developers, including the systemd package maintainer quit because of the stress\\
	\pause
	Lennart Poettering is a dick. \pause Allegedly\\
	\pause
	Community and developers don't get along well\\
	\pause
	Significantly different from previous systems in Linux\\\pause
	Change is scary\\
	\pause
	Large repository pulling in many smaller projects\\
	e.g. gummiboot was taken in to become systemd boot\\
	
	
\end{frame}

\section{Questions}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
